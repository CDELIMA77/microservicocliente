package br.com.mastertech.clienteserv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ClienteservApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienteservApplication.class, args);
	}

}
