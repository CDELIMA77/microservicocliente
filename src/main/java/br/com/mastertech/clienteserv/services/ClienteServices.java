package br.com.mastertech.clienteserv.services;

import br.com.mastertech.clienteserv.models.Cliente;
import br.com.mastertech.clienteserv.repositories.ClienteRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteServices {

    @Autowired
    private ClienteRepository clienteRepository;

    public Iterable<Cliente> buscarTodosClientes(List<Long> clienteId) {
        Iterable<Cliente> clienteIterable = clienteRepository.findAllById(clienteId);
        return clienteIterable;
    }

    public Iterable<Cliente> buscarTodosClientes() {
        Iterable<Cliente> cliente = clienteRepository.findAll();
        return cliente;
    }

    public Optional<Cliente> buscarPorId(Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        return clienteOptional;
    }

    public Cliente salvarCliente(Cliente cliente) {
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Cliente atualizarCliente(Cliente cliente) throws ObjectNotFoundException {
        Optional<Cliente> clienteOptional = buscarPorId(cliente.getId());

        if (clienteOptional.isPresent()) {
            Cliente clienteObjeto = clienteRepository.save(cliente);
            return clienteObjeto;
        }
        throw new ObjectNotFoundException(Cliente.class, "Cliente não cadastrado");
    }

    public void deletarCliente(Cliente cliente) {
        clienteRepository.delete(cliente);
    }

}