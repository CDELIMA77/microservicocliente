package br.com.mastertech.clienteserv.models;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "clientems")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome_completo")
    @Size(min=8, max=100, message = "Nome deve ter entre 8 e 100 caracteres")
    private String nome;

    public Cliente() {
    }

    public Cliente(Long id, @Size(min = 8, max = 100, message = "Nome deve ter entre 8 e 100 caracteres") String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}


