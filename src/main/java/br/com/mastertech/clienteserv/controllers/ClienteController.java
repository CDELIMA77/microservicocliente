package br.com.mastertech.clienteserv.controllers;

import br.com.mastertech.clienteserv.models.Cliente;
import br.com.mastertech.clienteserv.services.ClienteServices;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteServices clienteServices;

    @GetMapping
    public Iterable<Cliente> buscarTodosClientes(){
        return clienteServices.buscarTodosClientes();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Cliente> buscarCliente(@PathVariable Long id){
        Optional<Cliente> clienteOptional = clienteServices.buscarPorId(id);
        if (clienteOptional.isPresent()){
            return ResponseEntity.status(200).body(clienteOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<Cliente> incluirCliente(@RequestBody @Valid Cliente cliente) {
        Cliente clienteObjeto = clienteServices.salvarCliente(cliente);
        return ResponseEntity.status(201).body(clienteObjeto);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Cliente> atualizarCliente(@PathVariable Long id, @RequestBody @Valid Cliente cliente){
        cliente.setId(id);
        Cliente clienteObjeto ;
        try{ clienteObjeto = clienteServices.atualizarCliente(cliente);}
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(200).body(clienteObjeto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Cliente> deletarCliente(@PathVariable Long id){
        Optional<Cliente> clienteOptional = clienteServices.buscarPorId(id);
        if (clienteOptional.isPresent()) {
            clienteServices.deletarCliente(clienteOptional.get());
            return ResponseEntity.status(204).body(clienteOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}
