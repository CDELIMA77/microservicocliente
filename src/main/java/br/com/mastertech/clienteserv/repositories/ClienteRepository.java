package br.com.mastertech.clienteserv.repositories;

import br.com.mastertech.clienteserv.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}